# -*- coding: utf-8 -*-
import unittest
from Checkers import *

class TestChyecker(unittest.TestCase):
    
    def setUp(self):
        self.game = Checkers()
        self.jogador_x = 'x'
        self.jogador_o = 'o'

    def set_values(self, lin_ori, con_ori, lin_des, col_des):
        self.game.monta_coordenadas_validas()
        self.game.lin_peca_ori = lin_ori -1
        self.game.col_peca_ori = con_ori -1
        self.game.lin_peca_des = lin_des -1
        self.game.col_peca_des = col_des -1
        if self.game.verifica_peca_selecionada() and self.game.verifica_peca_destino():
            self.game.troca_posicoes()

    def test_jogada_invalidas(self):
        self.game.jogador_vez = 1
        self.set_values(2,2,4,2)
        tabuleiro = [['_','x','_','x','_','x','_','x'],
                     ['x','_','x','_','x','_','x','_'],
                     ['_','_','_','x','_','x','_','x'],
                     ['_','x','_','_','_','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['o','_','o','_','o','_','o','_'],
                     ['_','o','_','o','_','o','_','o'],
                     ['o','_','o','_','o','_','o','_']]
        self.assertNotEqual(self.game.tabuleiro,tabuleiro)

    def test_jogada(self):
        self.game.jogador_vez = 1
        self.set_values(3,2,4,3)
        tabuleiro = [['_','x','_','x','_','x','_','x'],
                     ['x','_','x','_','x','_','x','_'],
                     ['_','_','_','x','_','x','_','x'],
                     ['_','_','x','_','_','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['o','_','o','_','o','_','o','_'],
                     ['_','o','_','o','_','o','_','o'],
                     ['o','_','o','_','o','_','o','_']]
        self.assertEqual(self.game.tabuleiro,tabuleiro)
        self.game.jogador_vez = 2
        self.set_values(6,3,5,4)
        tabuleiro = [['_','x','_','x','_','x','_','x'],
                     ['x','_','x','_','x','_','x','_'],
                     ['_','_','_','x','_','x','_','x'],
                     ['_','_','x','_','_','_','_','_'],
                     ['_','_','_','o','_','_','_','_'],
                     ['o','_','_','_','o','_','o','_'],
                     ['_','o','_','o','_','o','_','o'],
                     ['o','_','o','_','o','_','o','_']]
        self.assertEqual(self.game.tabuleiro,tabuleiro)

    def test_jogada_obrigatoria(self):
        self.game.jogador_vez = 1
        self.set_values(3,2,4,1)
        self.game.jogador_vez = 2
        self.set_values(6,3,5,2)
        self.game.jogador_vez = 1
        self.set_values(3,4,4,5)
        tabuleiro = [['_','x','_','x','_','x','_','x'],
                     ['x','_','x','_','x','_','x','_'],
                     ['_','_','_','_','_','x','_','x'],
                     ['x','_','_','_','x','_','_','_'],
                     ['_','o','_','_','_','_','_','_'],
                     ['o','_','_','_','o','_','o','_'],
                     ['_','o','_','o','_','o','_','o'],
                     ['o','_','o','_','o','_','o','_']]
        self.assertNotEqual(self.game.tabuleiro,tabuleiro)
        self.game.jogador_vez = 1
        self.set_values(4,1,6,3)
        tabuleiro = [['_','x','_','x','_','x','_','x'],
                     ['x','_','x','_','x','_','x','_'],
                     ['_','_','_','x','_','x','_','x'],
                     ['_','_','_','_','_','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['o','_','x','_','o','_','o','_'],
                     ['_','o','_','o','_','o','_','o'],
                     ['o','_','o','_','o','_','o','_']]
        self.assertEqual(self.game.tabuleiro,tabuleiro)

    def test_fim_jogo(self):
        self.game.jogador_vez = 1
        self.set_values(3,2,4,1)

        self.game.jogador_vez = 2
        self.set_values(6,3,5,2)

        self.game.jogador_vez = 1
        self.set_values(4,1,6,3)

        self.game.jogador_vez = 2
        self.set_values(7,4,5,2)

        self.game.jogador_vez = 1
        self.set_values(3,4,4,5)

        self.game.jogador_vez = 2
        self.set_values(6,7,5,6)

        self.game.jogador_vez = 1
        self.set_values(4,5,6,7)

        self.game.jogador_vez = 2
        self.set_values(7,8,5,6)

        self.game.jogador_vez = 1
        self.set_values(3,6,4,5)

        self.game.jogador_vez = 2
        self.set_values(5,6,3,4)

        self.game.jogador_vez = 1
        self.set_values(2,5,4,3)

        self.game.jogador_vez = 2
        self.set_values(5,2,3,4)

        self.game.jogador_vez = 1
        self.set_values(2,3,4,5)

        self.game.jogador_vez = 2
        self.set_values(6,5,5,6)

        self.game.jogador_vez = 1
        self.set_values(4,5,6,7)

        self.game.jogador_vez = 2
        self.set_values(7,6,5,8)

        self.game.jogador_vez = 1
        self.set_values(3,8,4,7)

        self.game.jogador_vez = 2
        self.set_values(5,8,3,6)

        self.game.jogador_vez = 1
        self.set_values(2,7,4,5)

        self.game.jogador_vez = 2
        self.set_values(6,1,5,2)

        self.game.jogador_vez = 1
        self.set_values(1,6,2,5)

        self.game.jogador_vez = 2
        self.set_values(5,2,4,3)

        self.game.jogador_vez = 1
        self.set_values(1,4,2,3)

        self.game.jogador_vez = 2
        self.set_values(4,3,3,4)

        self.game.jogador_vez = 1
        self.set_values(2,5,4,3)

        self.game.jogador_vez = 2
        self.set_values(7,2,6,3)

        self.game.jogador_vez = 1
        self.set_values(2,3,3,2)

        self.game.jogador_vez = 2
        self.set_values(6,3,5,4)

        self.game.jogador_vez = 1
        self.set_values(4,5,6,3)

        self.game.jogador_vez = 2
        self.set_values(8,1,7,2)

        self.game.jogador_vez = 1
        self.set_values(6,3,8,1)

        self.game.jogador_vez = 2
        self.set_values(8,3,7,4)

        self.game.jogador_vez = 1
        self.set_values(4,3,5,4)

        self.game.jogador_vez = 2
        self.set_values(7,4,6,5)

        self.game.jogador_vez = 1
        self.set_values(5,4,7,6)

        self.game.jogador_vez = 2
        self.set_values(8,7,6,5)

        self.game.jogador_vez = 1
        self.set_values(3,2,4,3)

        self.game.jogador_vez = 2
        self.set_values(8,5,7,4)

        self.game.jogador_vez = 1
        self.set_values(2,1,3,2)

        self.game.jogador_vez = 2
        self.set_values(7,4,6,3)

        self.game.jogador_vez = 1
        self.set_values(3,2,4,1)

        self.game.jogador_vez = 2
        self.set_values(6,3,5,2)

        self.game.jogador_vez = 1
        self.set_values(4,1,6,3)

        self.game.jogador_vez = 2
        self.set_values(6,5,5,4)

        self.game.jogador_vez = 1
        self.set_values(4,3,6,5)
        tabuleiro = [['_','x','_','_','_','_','_','x'],
                     ['_','_','_','_','_','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['_','_','x','_','x','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['X','_','_','_','_','_','_','_']]
        self.assertEqual(self.game.tabuleiro,tabuleiro)


    def test_peca_captura_dama(self):

        self.game.jogador_vez = 1
        self.set_values(3,2,4,1)

        self.game.jogador_vez = 2
        self.set_values(6,5,5,6)

        self.game.jogador_vez = 1
        self.set_values(4,1,5,2)

        self.game.jogador_vez = 2
        self.set_values(6,3,4,1)

        self.game.jogador_vez = 1
        self.set_values(3,4,4,5)

        self.game.jogador_vez = 2
        self.set_values(5,6,3,4)

        self.game.jogador_vez = 1
        self.set_values(2,5,4,3)

        self.game.jogador_vez = 2
        self.set_values(7,4,6,5)

        self.game.jogador_vez = 1
        self.set_values(2,3,3,4)

        self.game.jogador_vez = 2
        self.set_values(4,1,3,2)

        self.game.jogador_vez = 1
        self.set_values(1,4,2,3)

        self.game.jogador_vez = 2
        self.set_values(3,2,1,4)

        self.game.jogador_vez = 1
        self.set_values(3,4,4,5)

        self.game.jogador_vez = 2
        self.set_values(1,4,2,5)

        self.game.jogador_vez = 1
        self.set_values(1,6,3,4)

        tabuleiro = [['_','x','_','_','_','_','_','x'],
                     ['x','_','_','_','_','_','x','_'],
                     ['_','_','_','x','_','x','_','x'],
                     ['_','_','x','_','x','_','_','_'],
                     ['_','_','_','_','_','_','_','_'],
                     ['o','_','_','_','o','_','o','_'],
                     ['_','o','_','_','_','o','_','o'],
                     ['o','_','o','_','o','_','o','_']]
        self.assertEqual(self.game.tabuleiro,tabuleiro)

if __name__ == '__main__':
    unittest.main()
