# -*- coding: utf-8 -*-

import Pyro.core
import sys
from config import *

class Client(object):

    def __init__(self, jogador):
        self.game = Pyro.core.getProxyForURI("PYROLOC://%s:%s/game" % (HOST,PORT))
        if not self.game.is_jogador_disponivel(jogador):
            print "Numero indisponivel"
            sys.exit()
        self.game.atribuir_jogador(jogador)
        self.meu_jogador = jogador

    def loop_jogo(self):
        """ 
            Verifica se o jogo terminou, e caso contrario verifica se eh a vez do jogador jogar ou se ele tem que esperar o adversario
        """
        while True:
            jogador_perdedor = self.game.get_jogador_perdedor()
            if jogador_perdedor:
                self.print_tabuleiro()
                if jogador_perdedor == self.meu_jogador:
                    print "Voce perdeu! =("
                else:
                    print "Voce venceu! =)"
                break

            if self.game.get_jogador_vez() == self.meu_jogador:
                self.print_tabuleiro()
                print "Sua vez, %s:\n" % (self.meu_nome())
                self.realiza_jogada()
            else:
                self.print_tabuleiro()
                print "Vez do adversario:\n"
                minha_vez = False
                while not minha_vez:
                    minha_vez = self.game.get_jogador_vez() == self.meu_jogador

    def realiza_jogada(self):
        """
            Realiza a jogada, pedindo e verificando as coordenadas iniciais e finais
        """
        enquanto = False
        while not enquanto:
            lin_peca_ori_temp = raw_input('Digite a linha da peca a ser movida:')
            col_peca_ori_temp = raw_input('Digite a coluna da peca a ser movida:')
            self.game.set_lin_peca_ori(lin_peca_ori_temp)
            self.game.set_col_peca_ori(col_peca_ori_temp)
            enquanto = self.game.verifica_peca_selecionada()
            if not enquanto:
                print 'Coordenada Invalida!'

        enquanto = False
        while not enquanto:
            lin_peca_des_temp = raw_input('Digite a linha da coordenada de destino:')
            col_peca_des_temp = raw_input('Digite a coluna da coordenada de destino:')
            self.game.set_lin_peca_des(lin_peca_des_temp)
            self.game.set_col_peca_des(col_peca_des_temp)
            enquanto = self.game.verifica_peca_destino()
            if not enquanto:
                print 'Coordenada Invalida!'

        continuar_jogando = self.game.troca_posicoes()
        retorno = continuar_jogando
        while continuar_jogando:
            #localiza as pecas disponiveis a partir da posicao atual
            if self.game.monta_coordenadas_locais():
                self.print_tabuleiro()
                if self.game.get_jogador_vez() == self.meu_jogador:
                    print "Sua vez, %s:\n" % (self.meu_nome())
                enquanto = False
                while not enquanto: 
                    lin_peca_des_temp = raw_input('Digite a linha da coordenada de destino:')
                    col_peca_des_temp = raw_input('Digite a coluna da coordenada de destino:')
                    self.game.set_lin_peca_des(lin_peca_des_temp)
                    self.game.set_col_peca_des(col_peca_des_temp)
                    enquanto = self.game.verifica_peca_destino()
                    if not enquanto:
                        print 'Coordenada Invalida!'
                continuar_jogando = self.game.troca_posicoes()
            else:
                continuar_jogando = False

        return retorno

    def print_tabuleiro(self):
        """
            Imprime o tabuleiro na tela
        """
        tabuleiro = self.game.get_tabuleiro()
        num = 1
        print '-----------------------------------------------------------'
        for linha in tabuleiro:
            if num < 10:
                print num, ' |',
            else:
                print num, '|',
            for quadrado in linha:
                print quadrado + ' |',
            print '\n'
            num = num + 1
        print '     1   2   3   4   5   6   7   8'

    def meu_nome(self):
        if self.meu_jogador == 1:
            return "X"
        else:
            return "O"

if __name__=="__main__":
    jogador = int(raw_input("Numero do jogador: 1 ou 2: "))
    client = Client(jogador)
    client.loop_jogo()
