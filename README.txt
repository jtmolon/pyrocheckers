Jogo de damas desenvolvido cliente/servidor.

Contributors:
    - Luciano Camargo Cruz    <luciano@lccruz.net>
    - João Toss Molon         <jtmolon@gmail.com>

License: 
    GPL 

Requisitos:
    Python >= 2.6
    Pyro

Rodar App:
    rodar o servidor:
        python server.py
    rodar o cliente:
        configurar o endereço de ip do servidor no config.py
        python cliente.py

Rodar Test:
    python tests.py
