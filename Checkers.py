# -*- coding: utf-8 -*-

import Pyro.core

class Checkers(Pyro.core.ObjBase):

    def __init__(self):
        super(Checkers, self).__init__()
        self.tabuleiro  = []
        self.jogador_x = 'x'
        self.jogador_o = 'o'
        self.jogador_vez = 2
        self.lista_pecas_disponiveis = []
        self.lin_peca_ori = 0
        self.col_peca_ori = 0
        self.lin_peca_des = 0
        self.col_peca_des = 0
        self.jogadores_disponiveis = [1, 2]
        self.jogador_perdedor = 0
        self.__iniciaTabuleiro()
        self.troca_jogador()

    def __iniciaTabuleiro(self):
        """
            Monta o tabuleiro com os valores iniciais
        """
        self.tabuleiro = [['_',self.jogador_x,'_',self.jogador_x,'_',self.jogador_x,'_',self.jogador_x],
                         [self.jogador_x,'_',self.jogador_x,'_',self.jogador_x,'_',self.jogador_x,'_'],
                         ['_',self.jogador_x,'_',self.jogador_x,'_',self.jogador_x,'_',self.jogador_x],
                         ['_','_','_','_','_','_','_','_'],
                         ['_','_','_','_','_','_','_','_'],
                         [self.jogador_o,'_',self.jogador_o,'_',self.jogador_o,'_',self.jogador_o,'_'],
                         ['_',self.jogador_o,'_',self.jogador_o,'_',self.jogador_o,'_',self.jogador_o],
                         [self.jogador_o,'_',self.jogador_o,'_',self.jogador_o,'_',self.jogador_o,'_']]

    def get_posicao_pecas(self, so_damas = False):
        lista_posicoes = []

        if self.jogador_vez == 1:
            if so_damas:
                simbolo = self.jogador_x.upper()
            else:
                simbolo = self.jogador_x
        else:
            if so_damas:
                simbolo = self.jogador_o.upper()
            else:
                simbolo = self.jogador_o

        linha_cont = 0
        for linha in self.tabuleiro:
            coluna_cont = 0
            for valor in linha:
                if valor == simbolo:
                    lista_posicoes.append((linha_cont,coluna_cont))
                coluna_cont += 1
            linha_cont += 1
        return lista_posicoes

    def get_tabuleiro(self):
        return self.tabuleiro

    def get_jogadores_disponiveis(self):
        return self.jogadores_disponiveis

    def is_jogador_disponivel(self, jogador):
        return jogador in self.jogadores_disponiveis

    def atribuir_jogador(self, jogador):
        if jogador in self.jogadores_disponiveis:
            self.jogadores_disponiveis.remove(jogador)

    def get_jogador_vez(self):
        return self.jogador_vez

    def set_lin_peca_ori(self, lin):
        self.lin_peca_ori = self.parse_lin_col(lin)

    def set_col_peca_ori(self, col):
        self.col_peca_ori = self.parse_lin_col(col)

    def set_lin_peca_des(self, lin):
        self.lin_peca_des = self.parse_lin_col(lin)

    def set_col_peca_des(self, col):
        self.col_peca_des = self.parse_lin_col(col)

    def get_lin_peca_ori(self):
        return self.lin_peca_ori + 1

    def get_col_peca_ori(self):
        return self.col_peca_ori + 1

    def get_lin_peca_des(self):
        return self.lin_peca_des + 1

    def get_col_peca_des(self):
        return self.col_peca_des + 1

    def get_jogador_perdedor(self):
        return self.jogador_perdedor

    def parse_lin_col(self, lin_col):
        try:
            lin_col = int(lin_col)-1
        except:
            lin_col = 0 
        return lin_col    

    def troca_posicoes(self):
        """ 
            Apos a jogada ser validada, troca as posicoes no tabuleiro
        """
        peca = self.lista_pecas_disponiveis[0]
        if peca['captura']:
            lin_captura, col_captura = peca['captura']
            self.tabuleiro[lin_captura][col_captura] = '_'

        self.tabuleiro[self.lin_peca_des][self.col_peca_des] = self.tabuleiro[self.lin_peca_ori][self.col_peca_ori]
        self.tabuleiro[self.lin_peca_ori][self.col_peca_ori] = '_'
    
        #verifica se o orelhudo vira dama
        virou_dama = self.verifica_dama()

        #retorna se houve captura e nao virou dama
        #um retorno True significa que o magrao pode jogar denovo
        #um retorno False significa que e a vez do outro orelhudo jogar        
        retorno = peca['captura'] and not virou_dama
        if retorno:
            self.lin_peca_ori = self.lin_peca_des
            self.col_peca_ori = self.col_peca_des
        else:
            self.troca_jogador()
        return retorno

    def valida_tipo(self, linha, coluna, tipo):
        if linha > 7 or coluna > 7 or linha < 0 or coluna < 0:
            return False
        if self.tabuleiro[linha][coluna] in [tipo, tipo.upper()]:
            return True
        else:
            return False

    def monta_coordenadas_validas(self):
        self.lista_pecas_disponiveis = []
        
        if self.jogador_vez == 1:
            capturar = 'o'
        else:
            capturar = 'x'
        i = 1

        #Testar damas que podem comer alguma peca
        for linha, coluna in self.get_posicao_pecas(so_damas = True):
            if self.valida_tipo(linha+i, coluna+i, capturar): #Frente diagonal direita + peca a capturar
                proximo = i+1
                if self.valida_tipo(linha+proximo, coluna+proximo, '_'): #Frente diagonal direita proximo i + 1
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha + proximo, coluna + proximo),
                            'captura': (linha + i, coluna + i)}
                    self.lista_pecas_disponiveis.append(peca)

            if self.valida_tipo(linha+i, coluna-i, capturar): #Frente diagonal esquerda + peca a capturar
                proximo = i+1
                if self.valida_tipo(linha+proximo, coluna-proximo, '_'): #Frente diagonal esquerda proximo i + 1
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha + proximo, coluna - proximo),
                            'captura': (linha + i, coluna - i)}
                    self.lista_pecas_disponiveis.append(peca)

            if self.valida_tipo(linha-i, coluna+i, capturar): #Frente diagonal direita + peca a capturar
                proximo = i+1
                if self.valida_tipo(linha-proximo, coluna+proximo, '_'): #Frente diagonal direita proximo i + 1
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha - proximo, coluna + proximo),
                            'captura': (linha - i, coluna + i)}
                    self.lista_pecas_disponiveis.append(peca)
            if self.valida_tipo(linha-i, coluna-i, capturar): #Frente diagonal esquerda + peca a capturar
                proximo = i+1
                if self.valida_tipo(linha-proximo, coluna-proximo, '_'): #Frente diagonal esquerda proximo i + 1
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha - proximo, coluna - proximo),
                            'captura': (linha - i, coluna - i)}
                    self.lista_pecas_disponiveis.append(peca)

        if self.lista_pecas_disponiveis:
            return True

        #Testar pecas comuns que podem comer alguma peca
        for linha, coluna in self.get_posicao_pecas():
            if capturar == 'o':
                if self.valida_tipo(linha+i, coluna+i, capturar): #Frente diagonal direita + peca a capturar
                    proximo = i+1
                    if self.valida_tipo(linha+proximo, coluna+proximo, '_'): #Frente diagonal direita proximo i + 1
                        peca = {'peca': (linha, coluna), 
                                'destino': (linha + proximo, coluna + proximo),
                                'captura': (linha + i, coluna + i)}
                        self.lista_pecas_disponiveis.append(peca)

                if self.valida_tipo(linha+i, coluna-i, capturar): #Frente diagonal esquerda + peca a capturar
                    proximo = i+1
                    if self.valida_tipo(linha+proximo, coluna-proximo, '_'): #Frente diagonal esquerda proximo i + 1
                        peca = {'peca': (linha, coluna), 
                                'destino': (linha + proximo, coluna - proximo),
                                'captura': (linha + i, coluna - i)}
                        self.lista_pecas_disponiveis.append(peca)
            else:
                if self.valida_tipo(linha-i, coluna+i, capturar): #Frente diagonal direita + peca a capturar
                    proximo = i+1
                    if self.valida_tipo(linha-proximo, coluna+proximo, '_'): #Frente diagonal direita proximo i + 1
                        peca = {'peca': (linha, coluna), 
                                'destino': (linha - proximo, coluna + proximo),
                                'captura': (linha - i, coluna + i)}
                        self.lista_pecas_disponiveis.append(peca)
                if self.valida_tipo(linha-i, coluna-i, capturar): #Frente diagonal esquerda + peca a capturar
                    proximo = i+1
                    if self.valida_tipo(linha-proximo, coluna-proximo, '_'): #Frente diagonal esquerda proximo i + 1
                        peca = {'peca': (linha, coluna), 
                                'destino': (linha - proximo, coluna - proximo),
                                'captura': (linha - i, coluna - i)}
                        self.lista_pecas_disponiveis.append(peca)

        if self.lista_pecas_disponiveis:
            return True

        #Testar as pecas que nao comem ninguem
        for linha, coluna in self.get_posicao_pecas():
            if capturar == 'o':
                if self.valida_tipo(linha+i, coluna+i, '_'): #Frente diagonal direita
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha + i, coluna + i),
                            'captura': False}
                    self.lista_pecas_disponiveis.append(peca)
                if self.valida_tipo(linha+i, coluna-i, '_'): #Frente diagonal esquerda
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha + i, coluna - i),
                            'captura': False}
                    self.lista_pecas_disponiveis.append(peca)
            else:
                if self.valida_tipo(linha-i, coluna+i, '_'): #Frente diagonal direita
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha - i, coluna + i),
                            'captura': False}
                    self.lista_pecas_disponiveis.append(peca)
                if self.valida_tipo(linha-i, coluna-i, '_'): #Frente diagonal esquerda
                    peca = {'peca': (linha, coluna), 
                            'destino': (linha - i, coluna - i),
                            'captura': False}
                    self.lista_pecas_disponiveis.append(peca)

        for linha, coluna in self.get_posicao_pecas(so_damas = True):
            if self.valida_tipo(linha+i, coluna+i, '_'): #Frente diagonal direita
                peca = {'peca': (linha, coluna), 
                        'destino': (linha + i, coluna + i),
                        'captura': False}
                self.lista_pecas_disponiveis.append(peca)
            if self.valida_tipo(linha+i, coluna-i, '_'): #Frente diagonal esquerda
                peca = {'peca': (linha, coluna), 
                        'destino': (linha + i, coluna - i),
                        'captura': False}
                self.lista_pecas_disponiveis.append(peca)
            if self.valida_tipo(linha-i, coluna+i, '_'): #Frente diagonal direita
                peca = {'peca': (linha, coluna), 
                        'destino': (linha - i, coluna + i),
                        'captura': False}
                self.lista_pecas_disponiveis.append(peca)
            if self.valida_tipo(linha-i, coluna-i, '_'): #Frente diagonal esquerda
                peca = {'peca': (linha, coluna), 
                        'destino': (linha - i, coluna - i),
                        'captura': False}
                self.lista_pecas_disponiveis.append(peca)

        if self.lista_pecas_disponiveis:
            return True

        #aqui o orelhudinho que esta jogando perdeu
        return False

    def monta_coordenadas_locais(self):
        """
            Montagem das coordenadas a partir da posicao atual, quando o jogador ja comeu alguma peca
        """
        #monta todas as coordenadas
        if self.monta_coordenadas_validas():
            #filtra pela peca selecionada
            if self.verifica_peca_selecionada():
                #filtra as que tem captura
                tem_captura = filter(lambda x: x['captura'], self.lista_pecas_disponiveis)
                if not tem_captura:
                    self.troca_jogador()
                return tem_captura

        self.troca_jogador()
        return False

    def verifica_peca_selecionada(self):
        """ Verifica peca selecionada na origem
        """
        na_lista = filter(lambda x: x['peca'] == (self.lin_peca_ori, self.col_peca_ori), self.lista_pecas_disponiveis)
        
        if na_lista:
            self.lista_pecas_disponiveis = na_lista
            return True

        return False

    def verifica_peca_destino(self):
        """ Verifica peca selecionada no destino
        """
        na_lista = filter(lambda x: x['destino'] == (self.lin_peca_des, self.col_peca_des), self.lista_pecas_disponiveis)
        
        if na_lista:
            self.lista_pecas_disponiveis = na_lista
            return True

        return False
                
    def verifica_dama(self):
        """
            Verifica se o jogador chegou ao extremo do tabuleiro para a peca virar dama
        """
        if self.tabuleiro[self.lin_peca_des][self.col_peca_des] in ['X','O']:
            return False
        dama = False
        if self.jogador_vez == 1:
            dama = self.lin_peca_des == 7
        else:
            dama = self.lin_peca_des == 0
        if dama:
            self.tabuleiro[self.lin_peca_des][self.col_peca_des] = self.tabuleiro[self.lin_peca_des][self.col_peca_des].upper()
        return dama

    def troca_jogador(self):
        """ 
            Alterna jogador
        """
        if self.jogador_vez == 1:
            self.jogador_vez = 2
        else:
            self.jogador_vez = 1
        if not self.monta_coordenadas_validas():
            self.jogador_perdedor = self.jogador_vez
        return self.jogador_vez

if __name__  == '__main__':
    game = Checkers()

    Pyro.core.initServer()
    daemon = Pyro.core.Daemon()
    uri = daemon.connect(game, "game")

    print "The daemon runs on port:", daemon.port
    print "The object's uri is:", uri

    daemon.requestLoop()
